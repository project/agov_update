
aGov Components
---------------

This module contains components for aGov which are not part of the core profile.

Currently its sole purpose is to provide update notifications.
